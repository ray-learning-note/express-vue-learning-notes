# API 路由建立
在上一節中建立了 Model，這一節要做的就是將 Controller 完成並接上 Model，如此就完成了第一個 API

## 新增路由並設定路由群組
打開 `app.js` 引入我們要使用的 api 路由
```js
const api = require('./routes/api');
```
路徑就設定成 `/api/v1` 吧
```js
app.use('/api/v1', api);
```
這下子所有進入 `/api/v1/` 以及底下路徑的所有請求都會進入 `/routes/api` 這個路由內

### 建立新路由
我們建立一個檔案 `/routes/api/index.js`
```js
const express = require('express');
const router = express.Router();
const productController = require('../../controllers/product');
const manufacturerController = require('../../controllers/manufacturer');

router.get('/manufacturers', manufacturerController.all);
router.get('/manufacturers/:id', manufacturerController.byId);
router.post('/manufacturers', manufacturerController.create);
router.put('/manufacturers/:id', manufacturerController.update);
router.delete('/manufacturers/:id', manufacturerController.remove);

router.get('/products', productController.all);
router.get('/products/:id', productController.byId);
router.post('/products', productController.create);
router.put('/products/:id', productController.update);
router.delete('/products/:id', productController.remove);

module.exports = router;
```
我們在上一步建立了路由群組，而這個步驟中我們照著經典的 RESTful API 規則來撰寫路由規則

- get：取得資源
- post：建立新資源
- put：更新資源
- delete：刪除資源

在這個路由群組中不難發現，我們引用了 `productController`、`manufacturerController` 這兩個尚未建立的檔案，而這兩個檔案就是我們 MVC 中的 Controller。而 `router` 物件接了各種路徑的請求方法，並指定給所屬的 Controller 方法

下個章節會建立 Controller 並且測試 API 是否成功運行