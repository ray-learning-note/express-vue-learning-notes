# 進入 MongoDB 的世界

## 安裝 MongoDB
因為使用的是 mac 筆電，所以最愛用 Homebrew 安裝東西，除了 node.js！這東西用 brew 裝會把你搞到懷疑人生，並且連官網都不建議這麼做

但是 MongoDB 官方有釋出[使用 Homebrew 的安裝方法](https://github.com/mongodb/homebrew-brew)，我們就這個做看看吧

至於不會使用 Homebrew 或是使用其他作業系統的人，可以使用[官方的下載中心](https://www.mongodb.com/download-center/community)進行安裝

安裝好之後有兩種啟動的方法
1. 作為背景執行：`mongod --config /usr/local/etc/mongod.conf --fork`
2. 使用 brew 管理

這邊推薦使用 brew 來操作的原因是容易操作，簡單幾個指令就可以
- 啟動服務：`brew services start mongodb-community`
- 重新啟動：`brew services restart mongodb-community`
- 停止服務：`brew services stop mongodb-community`

執行之後繼續輸入 `mongo` 指令，應該會進到 mongoDB 的指令列中
![hello-mongo](../../static/03/03-02-hello-mongo.png)
到這邊就代表已經 mongoDB 服務就安裝並且啟動了