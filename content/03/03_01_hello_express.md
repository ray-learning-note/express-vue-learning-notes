# 認識 Express
> ## 在這篇學到的事情：
> 使用 express-generator 建立專案，並了解主要資料夾架構
## 建立 Express 專案
使用 [express-generator](https://github.com/expressjs/generator) 來建立專案
```
npm install -g express-generator
```
安裝完成後可以使用以下指令查看版本是否有安裝成功
```
express --version
```
接著初始化建立新專案
```
express backend
```
建立之後運行看看初始樣貌
```
cd backend
npm install
npm start
```
打開瀏覽器 `http://localhost:3000/` 應該會看到以下畫面
![hello-express](../../static/03/03-01-hello-express.png)

打開看看 Express 專案的資料夾架構
- app.js：Express Web Application 原型
- routes/index.js：路由設定主要檔案
- views/：放 View 的檔案
- public/：放靜態檔案

和其它文章不同的是，這次使用 express-generator 建立的專案中並沒有把所有路由給寫在 `app.js` 中。而是根據不同的應用做拆分，這和前端使用 vue-router 的嵌套路由是相同的概念

### app.js
在 `app.js` 中一開始先對於會用到的資源進行導入，例如 `routes/index` 和 `routes/users` 兩個路由，也定義了專案使用的前端模板類型。但因為我們後端是作為 API 使用，所以沒什麼差

### routes/index.js
而在 `routes/index.js` 中定義了進入這個路由之後對於不同請求可以做的回應方式

這就是主要我們需要先了解的部分