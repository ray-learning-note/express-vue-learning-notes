# 認識 vuex

[vuex](https://vuex.vuejs.org/zh/) 來協助我們集中管理組件狀態，就不用到處維護狀態。使得組件只需要專注於渲染頁面，而前端狀態都保留在 vuex 中建立的狀態庫中。所謂狀態庫，其實也就是一個 Javascript 物件。


## 安裝 vuex

可以使用指令
```
npm install vuex --save
```
或是依然使用 `vue ui` 來安裝
![install-vuex](../../static/05/05-01-install-vuex.png)

如果是使用 `vue ui` 安裝，會發現專案下多了一個檔案：`/src/store/index.js`，先用以下內容取代原本的檔案。

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    // bought items
    cart: [],
    // ajax loader
    showLoader: false,
    // selected product
    product: {},
    // all products
    products: [
      {
        name: '產品 1',
      }
    ],
    // all manufacturers
    manufacturers: [],
  }
})
```

上面我們做了一些事情
- 導入了 `Vue` 和 `Vuex`
- 調用 `Vue.use` 方法宣告使用 `Vuex`，聽起來和 `vue-router` 的時候一樣
- 最後導出 `Vuex.Store` 物件，並傳入 `strict` 和 `state` 參數， `strict` 宣告我們必須使用 Vuex 的 Mutation 函數來改變 `state`，否則會報錯（這部分會在之後的章節說明）。而 `state` 參數儲存全域的狀態，比如在這邊定義了 `cart`、`showLoader` 等屬性，都是後面我們操作會使用到的資料。

## 整合 Vuex 和 Vue
當我們建立了 `Vuex` 的 `store` 物件後就可以使用它了，打開 `src/main.js` 在開頭導入之前建立的 `store`，並且將 `store` 添加到 Vue 初始化的參數列表裡

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

如果你是使用 `Vue ui` 安裝，打開檔案可以見到程式已經幫我們完成了以上的內容。當我們將 `store` 導入到了 Vue 中進行初始化後，Vue 會將 `store` 裡面的 `state` 注入到所有的組件中，這樣所有的 Vue 組件就共享了一個全域的 `state`，而所有的狀態變化都是針對這個 javascript 物件進行操作，然後觸法組件的重新渲染。同時也避免了組件嵌套層級過多時，組件之間傳遞屬性的複雜性，方便於大型程式與團隊合作。

## 查看結果
在整合好 Vuex 和 Vue 之後，馬上來看看 Vuex 帶來的變化。打開 `src/pages/admin/Products.vue` 修改成以下內容
```html
<template>
  <div>
    <div class="title">
      <h1>This is Admin</h1>
    </div>
    <div class="body">{{ product.name }}</div>
  </div>
</template>

<script>
export default {
  computed: {
    product() {
      return this.$store.state.products[0];
    }
  }
};
</script>
```

可以在 `<script>` 中看到一個沒見過的東西 `computed`，先來了解一下這是什麼

### Computed
如果在前端模板內加入太多的邏輯運算，不但可讀性差也難以維護，這問題在後端套版時常常發生，很難一眼看出它到底要渲染什麼。而 `計算屬性 (computed)` 有點類似於 method，但 method 需要括號來觸發。而且 method 不管相依的資料有沒有改變，都會重新計算。computed 則是在相依的資料有改變時才做計算並渲染，所以需要每次更新的行為用 method；在意效能的話使用 computed。

講完了 `computed` 屬性，回頭來看剛剛的改動。我們在 `computed` 定義了一個 `product` 屬性，會去 Vuex 也就是 `store` 中的 `state` 中 `products` 的第一個元素，然後在頁面上使用剛剛取到的內容中的 `name` 屬性渲染

於是我們知道可以使用 `this.$store.state` 來取得 Vuex store 中保存的 `state`